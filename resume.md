# 个人简历
![Yan](http://yans.qiniudn.com/Yan_Dong.png?imageView2/2/w/100 "Yan")

姓名 | 年龄 | 性别 | 所在地 | 毕业院校 | 学历 | 专业 | 毕业时间
--- | --- | --- | --- | --- | --- | --- | ---
董研 | 24 | 男 | 北京 | 华中科技大学 | 本科 | 光电 | 2012

## 项目经历
### _2009-2011_ ICPC集训队
- 学习各种竞赛算法
  * 搜索/模拟
  * 动态规划
  * 建模（数论，图论，网络流，模拟）
  * 基本数据结构（数组，链表，树，堆，栈，STL）
  * 数据表示结构（图的表示，字符串的表示）
- 参加比赛
  * 区域赛银牌
  * 邀请赛金牌

### _2010-2011_ 加入 Biff Networking 公司，参与创业项目
- 项目要做的是社交网络信息的自动聚合和分类，主要思想是根据用户选择的
几条消息，自动从消息流中过滤出有相同兴趣点的消息，显示。
- 团队构成
  * 一个北大自然语言实验室学生，负责算法技术
  * 百度员工，负责聚合算法调用，数据库，后台逻辑
  * 高中同学ashi, 负责美工，界面，html/css/js
  * 高中同学yy, biz-end, 技术外围工作
  * 我负责restful部分还有从twitter上抓取用户消息流的工作。restful
用php写成，抓取用户消息流用python写成。
- 项目最后上线运营了几周时间，最后由于经验不足，人员过于分散而失败

### _2012-2013_ 加入 MicroStrategy 工作
- MicroStrategy是传统BI公司，业务方向为生成数据的可视化报表并提供互操作性。
- 在公司加入SQL Engine组，负责维护生成SQL的逻辑代码。工作时使用Visual Studio
进行编程及debug。
- 负责维护业务逻辑，改bug，加feature，逻辑很复杂，递归性很强。
- 负责将windows项目移植到osx上，使用xcode创建项目，复用windows代码，使用宏将逻辑
分开执行
- 负责实现 MicroStrategy 内存数据库的查询语言CSI，通过 yacc/bison 将文本输入
转化为已有的内存结构，代码复用在windows平台和osx平台

### _2013-2014_ 和三个相熟之人合伙创办 James Black List Group Limited 公司（共四人）
> [电商网站-奢侈品方向](https://jamesblacklist.com/ "James Black List")

> [电商网站-代购方向](http://nono.co/ "NONOCO")

> [微广播](http://wei-broadcast.weichat.club/app "微广播")

- 公司方向是奢侈品电商网站，商品数据从国外若干知名网站获得
- 负责网页数据抓取工作
- 负责电商网站的架构设计与实现（商品数据库，用户系统，订单系统）
- 负责服务器配置(aws, heroku, mongohq, nginx)
- 负责数据库调优(mongodb 索引)
- 开发了微信营销系统 微广播


## 技术熟练程度
语言/技术/工具  | 使用时间 | 参与项目 | 熟练程度(1-5)
------------- | ------ | ---- | ----
C++  | >4yr | ICPC比赛，MicroStrategy工作 | 5
STL  | >4yr | ICPC比赛 | 5
boost| <1yr | 大学创业项目 | 2
mysql | <1yr | 大学创业项目 | 2
php | >1yr | 大学创业项目 | 3
python | <1yr | 大学创业项目 | 2
oauth | <1yr | 大学创业项目 | 2
visual studio | >1yr | MicroStrategy工作, 个人兴趣 | 3
xcode/c++ | <1yr | MicroStrategy工作 | 3
yacc/bison | <1yr | MicroStrategy工作 | 3
clearcase | 1yr | MicroStrategy工作 | 2
javascript | >4yr | 目前创业项目 | 5
html/css | >4yr | 目前创业项目 | 3
angularjs | <1yr | 目前创业项目 | 3
nodejs | 2yr | 目前创业项目 | 4
mongodb | 1yr | 目前创业项目 | 4
redis | <1yr | 目前创业项目 | 3
mocha | 1yr | 目前创业项目 | 4
git | 1yr | 目前创业项目 | 4
mocha | 1yr | 目前创业项目 | 3
shell | >1yr | 目前创业项目 | 3
SOA/RESTFul | 1yr | 目前创业项目 | 3
aws/heroku/mongohq | <1yr | 目前创业项目 | 3
grunt | 1yr | 目前创业项目 | 1
ruby | <1yr | 个人兴趣 | 2
erlang | <1yr | 个人兴趣 | 1
C# | <1yr | 个人兴趣 | 1
xml/SOAP | <1yr | 个人兴趣 | 1


